import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.parser.nndep.DependencyParser;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.tagger.common.Tagger;
import edu.stanford.nlp.trees.TypedDependency;

import edu.stanford.nlp.semgraph.SemanticGraph;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;
import java.util.List;
import java.io.*;

import com.csvreader.*;
import edu.stanford.nlp.trees.GrammaticalStructure;

class NLP{
    private StanfordCoreNLP coreNLP;
    private DependencyParser depParser;
    private Tagger tagger;
    NLP() throws IOException {
        Properties props = new Properties();
        props.load(IOUtils.readerFromString("myStanfordCoreNLP-chinese.properties"));
        coreNLP = new StanfordCoreNLP(props);
        depParser = DependencyParser.loadFromModelFile("edu/stanford/nlp/models/parser/nndep/UD_Chinese.gz");
        tagger = Tagger.loadModel("edu/stanford/nlp/models/pos-tagger/chinese-distsim/chinese-distsim.tagger");
    }

    private CoreDocument annotate(String text){
        CoreDocument doc = new CoreDocument(text);
        coreNLP.annotate(doc);
        return doc;
    }

    String proc_noSeg(String text){
        String[] sen = text.split(" ");
        Map<Integer, Integer> widx2cidx = new HashMap<>();
        int cidx = 0;
        int i = 1;
        ArrayList<HasWord> sen_hw = new ArrayList<HasWord>();
        for(String tok :sen){
            sen_hw.add(new Word(tok));
            widx2cidx.put(i, cidx);
            cidx += tok.toCharArray().length;
            i += 1;
        }
        assert widx2cidx.size() == sen.length;

        List<String> deps = new ArrayList<>();
        List<String> heads = new ArrayList<>();
        List<String> heads_ch = new ArrayList<>();

        List<TaggedWord> tagged = tagger.apply(sen_hw);
        GrammaticalStructure gs = depParser.predict(tagged);
        for (TypedDependency edge: gs.typedDependencies()){
            IndexedWord node = edge.dep();
            IndexedWord parent = edge.gov();
            String dep = edge.reln().toString();
            int head;
            List<String> head_ch = new ArrayList<>();
            if(parent.word() == null) {
                dep = "root";
                head = -1;
                for(char ignored :node.word().toCharArray())
                    head_ch.add("-1");
            }
            else{
                head = parent.index() - 1;
                for(char ignored :node.word().toCharArray())
                    head_ch.add(widx2cidx.get(parent.index()).toString());
            }

            deps.add(dep);
            heads.add(head + "");
            heads_ch.add(String.join(" ", head_ch));
        }

        return String.join(" ", sen) + "\t" +
                String.join(" ", deps) + "\t" +
                String.join(" ", heads) + "\t" +
                String.join(" ", heads_ch);

    }

    String[] proc(String text){
        CoreDocument doc = annotate(text);
        Map<Integer, Integer> widx2cidx = new HashMap<>();
        List<String> toks = new ArrayList<>();
        List<String> tags = new ArrayList<>();
        List<String> tags_ch = new ArrayList<>();
        int cidx = 0;
        int base = 0;
        for(CoreSentence sen :doc.sentences()) {
            for(CoreLabel tok :sen.tokens()){
                toks.add(tok.originalText());
                tags.add(tok.tag());
                widx2cidx.put(base + tok.index(), cidx);
                cidx += tok.originalText().toCharArray().length;
                for(char ignored :tok.originalText().toCharArray())
                    tags_ch.add(tok.tag());
            }
            base += sen.tokens().size();
        }
        assert widx2cidx.size() == toks.size();

        List<String> deps = new ArrayList<>();
        List<String> heads = new ArrayList<>();
        List<String> heads_ch = new ArrayList<>();

        base = 0;
        for(CoreSentence sen :doc.sentences()){
            SemanticGraph graph = sen.dependencyParse();
            for(IndexedWord node :graph.vertexListSorted()){
                IndexedWord parent = graph.getParent(node);
                String dep;
                int head;
                List<String> head_ch = new ArrayList<>();
                if(parent == null) {
                    dep = "root";
                    head = -1;
                    for(char ignored :node.originalText().toCharArray()) {
                        head_ch.add("-1");
                    }
                }
                else{
                    dep = graph.reln(parent, node).toString();
                    head = parent.index() - 1  + base;
                    for(char ignored :node.originalText().toCharArray())
                        head_ch.add(widx2cidx.get(base + parent.index()).toString());
                }

                deps.add(dep);
                heads.add(head + "");
                heads_ch.add(String.join(" ", head_ch));
            }
            base += sen.tokens().size();
        }

        return new String[]{String.join(" ", toks),
                String.join(" ", tags),
                String.join(" ", tags_ch),
                String.join(" ", deps),
                String.join(" ", heads),
                String.join(" ", heads_ch)};
    }

    private String[] get_em(String[] arr_from, String[] arr_to){
        Set<String> set_to = new HashSet<>();
        set_to.addAll(Arrays.asList(arr_to));
        List<String> res = new ArrayList<>();
        for(String item:arr_from){
            if(set_to.contains(item))
                res.add("1");
            else
                res.add("0");
        }
        String[] res_strs = new String[res.size()];
        res.toArray(res_strs);
        return res_strs;
    }


    String[] proc_para(String text1, String text2){
        String[] res1 = proc(text1);
        String[] res2 = proc(text2);
        String[] toks1 = res1[0].split(" ");
        String[] toks2 = res2[0].split(" ");
        String tags1 = res1[1];
        String tags2 = res2[1];
        String tags1_ch = res1[2];
        String tags2_ch = res2[2];
        String deps1 = res1[3];
        String deps2 = res2[3];
        String heads1 = res1[4];
        String heads2 = res2[4];
        String heads1_ch = res1[5];
        String heads2_ch = res2[5];

        List<String> chs1 = new ArrayList<>();
        List<String> chs2 = new ArrayList<>();
        for(String tok:toks1)
            for(char ch:tok.toCharArray())
                chs1.add(ch + "");
        for(String tok:toks2)
            for(char ch:tok.toCharArray())
                chs2.add(ch + "");
        String[] chs1_strs = new String[chs1.size()];
        String[] chs2_strs = new String[chs2.size()];
        chs1.toArray(chs1_strs);
        chs2.toArray(chs2_strs);

        String[] ems1 = get_em(toks1, toks2);
        String[] ems2 = get_em(toks2, toks1);
        String[] ems1_ch = get_em(chs1_strs, chs2_strs);
        String[] ems2_ch = get_em(chs2_strs, chs1_strs);
        return new String[]{
                String.join(" ", toks1),
                deps1, heads1, heads1_ch, String.join(" ", ems1), String.join(" ", ems1_ch),
                tags1, tags1_ch,
                String.join(" ", toks2),
                deps2, heads2, heads2_ch, String.join(" ", ems2), String.join(" ", ems2_ch),
                tags2, tags2_ch};
    }

}


public class Main {

    private static void proc_ifeng(String fpath) throws IOException {
        NLP nlp = new NLP();
        CsvReader reader = new CsvReader(fpath, ',', Charset.forName("UTF-8"));
        BufferedWriter fout = new BufferedWriter(new FileWriter(fpath + ".tok.dep"));
        ArrayList<String[]> lines_csv = new ArrayList<String[]>();
        while (reader.readRecord())
            lines_csv.add(reader.getValues());
        reader.close();

        int i = 0;
        String inp = null;
        for (String[] items:lines_csv)
            try {
                String lbl = items[0];
                String title = items[1];
                String fp = items[2];
                inp = title + " " + fp;
                String[] res = nlp.proc(inp);
                String toks = res[0];
                String deps = res[3];
                String heads = res[4];
                String heads_ch = res[5];
                String[] line_items = {lbl, toks, deps, heads, heads_ch, "\n"};
                String line = String.join("\t", line_items);
                fout.write(line);
                System.out.println(i + "/" + lines_csv.size());
                i += 1;
            } catch (Exception e) {
                System.out.println(e.toString());
                System.out.println(inp);
            }
        fout.close();
    }

    private static void proc_LCQMC(String fpath) throws IOException {
        NLP nlp = new NLP();
        BufferedReader fin = new BufferedReader(new FileReader(fpath));
        BufferedWriter fout = new BufferedWriter(new FileWriter(fpath + ".tok.dep"));
        ArrayList<String[]> lines = new ArrayList<>();
        String line;
        while ((line = fin.readLine()) != null)
            lines.add(line.trim().split("\t"));
        fin.close();

        int i = 0;
        for (String[] items:lines)
            try {
                String sen0 = items[0];
                String sen1 = items[1];
                String lbl = items[2];
                line = lbl + "\t" + String.join("\t", nlp.proc_para(sen0, sen1) ) + "\n";
                fout.write(line);
                System.out.println(i + "/" + lines.size());
                i += 1;
            } catch (Exception e) {
                System.out.println(e.toString());
                System.out.println(line);
            }
        fout.close();
    }

    private static void proc_MNRE(String fpath) throws IOException {
        NLP nlp = new NLP();
        BufferedReader fin = new BufferedReader(new FileReader(fpath));
        BufferedWriter fout = new BufferedWriter(new FileWriter(fpath + ".tok.dep"));
        ArrayList<String[]> lines = new ArrayList<>();
        String line;
        while ((line = fin.readLine()) != null)
            lines.add(line.trim().split("\t"));
        fin.close();

        int i = 0;
        String inp = null;
        for (String[] items:lines)
            try {
                String eid0 = items[0];
                String eid1 = items[1];
                String estr0 = items[2];
                String estr1 = items[3];
                String rela = items[4];
                inp = items[5].replace("###END###", "").trim();
                line = eid0 + "\t" + eid1 + "\t" + estr0 + "\t" + estr1 + "\t" + rela + "\t" + nlp.proc_noSeg(inp) + "\n";
                fout.write(line);
                System.out.println(i + "/" + lines.size());
                i += 1;
            } catch (Exception e) {
                System.out.println(e.toString());
                System.out.println(inp);
            }
        fout.close();
    }

    public static void main(String[] args) throws IOException {
//        String text = "克林顿说，华盛顿将逐步落实对韩国的经济援助。"
//                + "金大中对克林顿的讲话报以掌声：克林顿总统在会谈中重申，他坚定地支持韩国摆脱经济危机。";
//        NLP nlp = new NLP();
//        String res = nlp.proc_noSeg("香港 商报 传媒 副 总裁 、 大连市 政府 顾问 兰晓华 ： “ 小 熊 是 一个 有 想法 有 见地 的 实战 策划 高手 。");
//        System.out.println(res);
        proc_LCQMC("data/LCQMC/train.txt");
        proc_LCQMC("data/LCQMC/dev.txt");
        proc_LCQMC("data/LCQMC/test.txt");

//        proc_MNRE("data/MNRE/train_zh.txt");
//        proc_MNRE("data/MNRE/valid_zh.txt");
//        proc_MNRE("data/MNRE/test_zh.txt");
//        String text = "巴拉克·奥巴马是美国总统。他在2008年当选?今年的美国总统是特朗普？普京的粉丝";
//        String text = "N本报记者 林小明 通讯员 邓享勇";
//        String res = nlp.proc(text);
//        text = "N本报记者 陈世国 通讯员 康志强";
//        nlp.proc(text);
//        text = "N本报记者 郑建彬 实习生 柯文浩通讯员 林勇";
//        nlp.proc(text);
//        text = "N本报记者 吴臻 毛朝青 文/图";
//        nlp.proc(text);

//        System.out.println(res);
//        proc_ifeng("data/ifeng/train.csv");
//        proc_ifeng("data/ifeng/test.csv");
//
    }
}
